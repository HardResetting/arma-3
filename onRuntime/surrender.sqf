_curScreenPos = screenToWorld [0.5,0.5];
_x1 = screenToWorld [0.5,0.5] select 1;
_x2 = screenToWorld [0.5,1] select 1;
_radius = _x1 - _x2;
hint format ["%1, %2, %3", _x1, _x2, _radius];

_nearMen = _curScreenPos nearEntities ["Man", _radius];

{
	removeAllWeapons _x;
	removeBackpack _x;
	_x action ["Surrender", _x];
	_x Setcaptive true;
} forEach (_nearMen - [player]);