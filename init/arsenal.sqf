_crate = _this select 0;

[_crate,
	[
		"B_AssaultPack_blk",
		"B_AssaultPack_sgg",
		"B_AssaultPack_dgtl",
		"B_Carryall_oli",
		"B_Kitbag_sgg",
		"B_Kitbag_cbr",
		"B_Messenger_Black_F"
	] 
,true] call BIS_fnc_addVirtualBackpackCargo; 


[_crate,
	[
		"U_B_HeliPilotCoveralls",
		"U_B_Wetsuit",
		"U_C_Journalist",
		"U_Rangemaster",
		"U_B_CombatUniform_wdl_tshirt",
		"U_I_G_resistanceLeader_F",
		"U_I_OfficerUniform",
		"U_I_CombatUniform",
		"V_Rangemaster_belt",
		"V_TacVestIR_blk",
		"V_RebreatherB",
		"V_TacVest_blk_POLICE",
		"H_Beret_02",
		"H_Beret_Colonel",
		"H_Cap_police",
		"H_PilotHelmetHeli_B",
		"H_HelmetSpecB",
		"NVGoggles_OPFOR",
		"Binocular",
		"optic_Aco",
		"optic_ACO_grn",
		"optic_Aco_smg",
		"optic_ACO_grn_smg",
		"optic_DMS_ghex_F",
		"optic_MRCO",
		"optic_Hamr",
		"acc_flashlight",
		"acc_flashlight_smg_01",
		"acc_flashlight_pistol",
		"acc_pointer_IR",
		"muzzle_snds_H",
		"muzzle_snds_L",
		"muzzle_snds_M",
		"muzzle_snds_B",
		"muzzle_snds_H_MG",
		"muzzle_snds_H_SW",
		"G_Aviator",
		"G_B_Diving",
		"G_Spectacles",
		"G_Squares",
		"G_Squares_Tinted"
	] 
,true] call BIS_fnc_addVirtualItemCargo; 


[_crate,
	[
		"%All"
	] 
,true] call BIS_fnc_addVirtualMagazineCargo; 


[_crate,
	[
		"SMG_03_TR_black",
		"arifle_CTAR_blk_F",
		"arifle_Katiba_F",
		"arifle_Mk20_F",
		"arifle_MX_khk_F",
		"hgun_PDW2000_F",
		"srifle_DMR_01_F",
		"arifle_SDAR_F",
		"SMG_02_F",
		"srifle_GM6_F",
		"arifle_SPAR_01_blk_F",
		"launch_NLAW_F",
		"hgun_P07_F",
		"hgun_P07_khk_F",
		"hgun_Rook40_F",
		"hgun_ACPC2_F",
		"hgun_Pistol_heavy_01_F",
		"hgun_Pistol_01_F"
	] 
,true] call BIS_fnc_addVirtualWeaponCargo; 
